
Pod::Spec.new do |s|
  s.name             = 'Digius'
  s.version          = '1.0.1'
  s.summary          = 'Securely fetch documents from DigiLocker'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/digius-ios'
  s.license          = 'MIT'
  s.author           = { 'sravani' => 'shravani@frslabs.com' }
  s.source           = { :http => 'https://www.repo2.frslabs.space/repository/digius-ios/digius-ios/1.0.1/Digius.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Digius.framework'
  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
end